Dieses Werk ist unter einer Creative Commons Lizenz vom Typ
Namensnennung-Nicht kommerziell 3.0 Unported zugänglich.
Um eine Kopie dieser Lizenz einzusehen, konsultieren Sie
http://creativecommons.org/licenses/by-nc/3.0/ oder wenden
Sie sich brieflich an Creative Commons, Postfach 1866,
Mountain View, California, 94042, USA.
