====================
Selectoren -- Klasse
====================


.. _20230204T211611:

.. INDEX:: Selectoren; Klasse 
.. INDEX:: Klasse; Selectoren 

Klassendefiniton
----------------

.. code:: css


    .meine-klasse { backround: red;}

Zuordnung (eine Klasse)
-----------------------

.. code:: html


    <p class="meine-klasse"> Inhalt formatiert mit dem CSS-Selector: .meine-klasse </p>

Zuordnung (diverse Klassen)
---------------------------

Einem Element kann eine Liste von Klassen zugeordnet werden.

Beispiel bei Verwendung des Tachyons-Frameworks:

.. code:: html


    <article class="pa3 pa5-ns">
       <h1 class="f3 f1-m f-headline-l">Title</h1>
       <p class="measure lh-copy">
         Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
         nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
         erat, sed diam voluptua. At vero eos et accusam et justo duo
         dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
         sanctus est Lorem ipsum dolor sit amet.
      </p>
      <p class="measure lh-copy">
         Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
         nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam
         erat, sed diam voluptua. At vero eos et accusam et justo duo
         dolores et ea rebum. Stet clita kasd gubergren, no sea takimata
         sanctus est Lorem ipsum dolor sit amet.
      </p>
    </article>
