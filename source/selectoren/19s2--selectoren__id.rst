================
Selectoren -- ID
================


.. _20230204T214621:

.. INDEX:: Selektoren; ID
.. INDEX:: ID; Selektoren

Definition für die ID
---------------------

.. code:: css


    #nav { backround: red;}

Zuordnung
---------

ID's sind eindeutige Zuordnungen und existeren nur einmal pro Webseite.

.. code:: html


    <p id="nav">Inhalt der Navigation</p>

Alternative: nav
----------------

Statt der ID kann auch der Elementname »nav« eingesetzt werden. 

.. code:: html


    <nav>Inhalt der Navigation</nav>

Die Zuornung erfolgt dann wieder über die Selection des Elemnent-Namens.

.. code:: css


    nav { backround: red;}
