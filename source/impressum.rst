.. _impressum:

===========
 Impressum
===========

.. |c| image:: ./_static/p-im-fernseher.jpg
       :width: 0	    


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../_static/p-im-fernseher.jpg')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../_static/p-im-fernseher.jpg">
   </a>


.. sidebar::  Über mich

   | |b|

|a|


Peter Koppatz

Schmerberger Weg 92a

14548 Schwielowsee


Wortmeldugen gern an:

.. image:: ./_static/adresse.svg

============
Kursmaterial
============

Das Schulungsmaterial kann zum Nachschlagen, für das Selbstlernen und
andere Schulungskonzepte verwendet werden. Die OER-Lizenz bedeutet,
dass mit den Inhalten folgende Aktionen durchgeführt werden dürfen:

- ungefragt verwenden
- ungefragt verändern
- ungefragt mit anderen Inhalten neu vermischen

Es gelten auch die Regeln der Creative Commons Lizenz: 

.. raw:: html

   <a rel="license"
      href="http://creativecommons.org/licenses/by/4.0/">
      <img alt="Creative Commons Lizenzvertrag" style="border-width:0"
      src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />
      Dieses Werk ist lizenziert unter der <br />
      <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
      Creative Commons Namensnennung 4.0 International Lizenz</a>.

Sollten diese Lizenzen immer noch für Unsicherheiten bezüglich der
Nutzung hervorrufen, erkläre ich hiermit einen Nichtangriffspackt und
verzichte auf alle rechtlichen Mittel, da es nichts durchzusetzen gibt.
